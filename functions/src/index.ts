import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import * as bodyParser from "body-parser";

admin.initializeApp(functions.config().firebase);
const db = admin.firestore(); // Add this

const app = express();
const main = express();

main.use('/api/v1', app);
main.use(bodyParser.json());

export const webApi = functions.https.onRequest(main);

app.get('/warmup', (request, response) => {

    response.send('Warming up friend.');

});


app.get('/students', async (_request, response) => {
    try {

        const students: any[] = []
        const studentBranchReferences = await db
            .collection("college")
            .doc("students")
            .listCollections();

        for (const collection of studentBranchReferences) {
            const branch = await collection.get();
            branch.forEach((document) => {
                students.push(document.data())
            })
        }

        response.json(students)


    } catch (error) {
        response.status(500).send(error);
    }

});
app.post('/students', async (request, response) => {
    try {
        const {rollno, year, section, branch} = request.body;
        const data = {rollno, year, section, branch};

        const studentReference = db
            .collection('college')
            .doc('students')
            .collection(branch.toString().toUpperCase())
            .doc(rollno);

        await studentReference.set(data);
        const student = await studentReference.get();

        response.json(student.data())

    } catch (error) {
        response.status(500).send(error);
    }
});
app.get('/students/branch/:branch', async (request, response) => {
    try {
        const students: any[] = [];
        const branch = request.params.branch;
        if (!branch) response.send("Branch is required to receive the data")

        const branchReference = await db
            .collection('college')
            .doc('students')
            .collection(branch.toString().toUpperCase())
            .get();

        if (branchReference.docs.length == 0) {
            response.send(`${branch.toUpperCase()} does not exist`)
        }

        branchReference.forEach((document) => {
            students.push(document.data())
        });
        response.json(students)
    } catch (error) {
        response.status(500).send(error);
    }
});

app.get('/blocks', async (_request, response) => {
    try {

        const allTheRooms: any[] = [];

        const collectionBlocksReferences = await db.collection("college")
            .doc("blocks")
            .listCollections();

        for (const collection of collectionBlocksReferences) {
            const block = await collection.get();
            block.forEach((document) => {
                allTheRooms.push(document.data())
            })
        }

        response.json(allTheRooms)


    } catch (error) {

        response.status(500).send(error);

    }

});
app.post('/blocks', async (request, response) => {
    try {
        const {roomnumber, block, rows, benchesInRow} = request.body;
        const data = {roomnumber, block, rows, benchesInRow};

        const roomReference = db
            .collection('college')
            .doc('blocks')
            .collection(block.toString().toUpperCase())
            .doc(roomnumber);

        await roomReference.set(data);
        const room = await roomReference.get();

        response.json(room.data())

    } catch (error) {
        response.status(500).send(error);
    }
});
app.get('/blocks/:block', async (request, response) => {
    try {
        const rooms: any[] = [];
        const block = request.params.block;
        if (!block) response.send("Block Name is required to receive the data")

        const blockReference = await db
            .collection('college')
            .doc('blocks')
            .collection(block.toString().toUpperCase())
            .get();

        if (blockReference.docs.length == 0) {
            response.send(`${block.toUpperCase()} does not exist`)
        }

        blockReference.forEach((document) => {
            rooms.push(document.data())
        });
        response.json(rooms)
    } catch (error) {
        response.status(500).send(error);
    }
});


// app.get('/students/:id', async (request, response) => {
//     try {
//         const studentId = request.params.id;
//
//         if (!studentId) throw new Error('Fight ID is required');
//
//         const student = await db.collection('students').doc(studentId).get();
//
//         if (!student.exists) {
//             throw new Error('Fight doesnt exist.')
//         }
//
//         response.json({
//             id: student.id,
//             data: student.data()
//         });
//
//     } catch (error) {
//
//         response.status(500).send(error);
//
//     }
// });
// app.put('/students/:id', async (request, response) => {
//     try {
//
//         const studentId = request.params.id;
//         const rollno = request.body.rollno;
//
//         if (!studentId) throw new Error('id is blank');
//
//         if (!rollno) throw new Error('Rollno is required');
//
//         const data = {
//             rollno: rollno
//         };
//         await db.collection('students')
//             .doc(studentId)
//             .set(data, {merge: true});
//
//         response.json({
//             id: studentId,
//             data
//         })
//
//
//     } catch (error) {
//
//         response.status(500).send(error);
//
//     }
//
// });
//
// app.delete('/students/:id', async (request, response) => {
//     try {
//
//         const studentId = request.params.id;
//
//         if (!studentId) throw new Error('id is blank');
//
//         await db.collection('students')
//             .doc(studentId)
//             .delete();
//
//         response.json({
//             id: studentId,
//         })
//
//
//     } catch (error) {
//
//         response.status(500).send(error);
//
//     }
//
// });
